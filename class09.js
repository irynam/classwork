window.onload = ()=>{
    let btnStart = document.getElementById("Start");
    let btnStop = document.getElementById("Stop");
    let btnPause = document.getElementById("Pause");
    let output = document.getElementById("output");

    let myTimer;
    let counter = 0;

    output.innerHTML = `${msToHMS(counter * 100)}`

    btnStart.onclick = function() {
        myTimer = window.setInterval(() => {
            counter++;
            output.className = "class1";
            output.innerHTML = `${msToHMS(counter * 100)}`
        }, 100)
    }

    btnStop.onclick = function() {
        window.clearTimeout(myTimer);
        counter = 0;
        output.className = "";
        output.innerHTML = `${msToHMS(counter * 100)}`;
    }

    btnPause.onclick = function() {
        output.className = "class2";
        window.clearTimeout(myTimer);
    }

    function msToHMS(duration) {
        let seconds = parseInt((duration/1000)%60)
            , minutes = parseInt((duration/(1000*60))%60)
            , hours = parseInt((duration/(1000*60*60))%24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        let res = `${hours} : ${minutes} : ${seconds}`
        return(res);
    }
}